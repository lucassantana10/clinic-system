Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  resources :pacients
  resources :services
  resources :specialties
  resources :dashboard, only: [:index]
  resources :appointments, only: [:index, :create, :update]


  get 'appointment/:id' => 'appointments#appointment_start', as: :appointment_start
  get 'appointment/cancel/:id' => 'appointments#appointment_cancel', as: :appointment_cancel
  get 'appointment/payment/:id' => 'appointments#appointment_payment', as: :appointment_payment
  patch 'appointment/payment/success/:id' => 'appointments#appointment_update', as: :appointment_update
  get 'appointments/finished-of-day' => 'appointments#appointment_finished', as: :appointment_finished
  get 'appointment/detail/:id' => 'appointments#appointment_detail', as: :appointment_detail
  get 'appointments/history/:pacient_id' => 'appointments#appointment_history_all', as: :appointments_history_all
  get 'appointments/of-day/:user_id' => 'appointments#appointments_of_day', as: :appointments_of_day
  get 'appointments/edit-before-pay/:id' => 'appointments#edit_before_pay', as: :edit_before_pay

root 'passthrough#index'
end
