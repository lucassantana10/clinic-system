# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
p 'Create Roles'
Role.find_or_create_by(name: 'Administrator')
Role.find_or_create_by(name: 'Médico')
Role.find_or_create_by(name: 'Dentista')
Role.find_or_create_by(name: 'Laboratorio')
Role.find_or_create_by(name: 'Recepcionista')

p 'Create Gender'
Gender.find_or_create_by(name: 'Masculino')
Gender.find_or_create_by(name: 'Feminino')
Gender.find_or_create_by(name: 'Outro')

p 'Create Specialities'

Specialty.find_or_create_by(name: 'Desenvolvedor', description: 'Desenvolvedor')
Specialty.find_or_create_by(name: 'Ortodontia', description: 'Pessoa responsável por corrigir a posição dos dentes e dos ossos maxilares posicionados de forma inadequada.')
Specialty.find_or_create_by(name: 'Atendimento', description: 'Pessoa responsável pela recepção dos pacientes, orientando eles a consulta desejada.')

p 'Create Admin User'

  User.create(name: 'Lucas Moura', email: 'lucas.moura.san@gmail.com',
              password: '#LUCASmoura2018#01',
              password_confirmation: '#LUCASmoura2018#01', role_id: 1, specialty_id: 1, gender_id: 1, avatar: 'owner-developer.png')

  User.create(name: 'Adelita Leal', email: 'adelitaluana@gmail.com',
              password: 'adelitaconsultorio2019',
              password_confirmation: 'adelitaconsultorio2019', role_id: 3, specialty_id: 2, gender_id: 2, avatar: 'doctor-female.png')

  User.create(name: 'Recepção', email: 'lealconsultorios@gmail.com',
              password: '#consultorio2019#',
              password_confirmation: '#consultorio2019#', role_id: 5, specialty_id: 3, gender_id: 2, avatar: 'receptionist-female.png')


p 'Finished'
