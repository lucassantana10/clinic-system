class CreateAppointmentServices < ActiveRecord::Migration[5.2]
  def change
    create_table :appointment_services do |t|
      t.integer :appointment_id
      t.integer :service_id

      t.timestamps null: false
    end
    add_index :appointment_services, :service_id
    add_index :appointment_services, :appointment_id
  end
end
