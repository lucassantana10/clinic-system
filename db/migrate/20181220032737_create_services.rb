class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.integer :specialty_id

      t.timestamp :deleted_at
      t.timestamps null: false
    end
    add_index :services, :specialty_id
  end
end
