class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.integer :specialty_id
      t.integer :pacient_id
      t.integer :service_id
      t.integer :user_id
      t.string :status
      t.decimal :price_optional
      t.string :payment_type
      t.boolean :payment_status
      t.text :note

      t.timestamps null: false
    end
    add_index :appointments, :specialty_id
    add_index :appointments, :pacient_id
    add_index :appointments, :service_id
    add_index :appointments, :user_id
  end
end
