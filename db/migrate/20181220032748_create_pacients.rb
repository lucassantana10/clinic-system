class CreatePacients < ActiveRecord::Migration[5.2]
  def change
    create_table :pacients do |t|
      t.string :name
      t.string :cpf
      t.string :rg
      t.string :address
      t.string :number
      t.string :complement
      t.string :cep
      t.string :profession
      t.string :fone
      t.string :celphone
      t.string :email
      t.integer :gender_id
      t.date :birthday
      t.string :marital_status
      t.string :nacionality
      t.string :indication

      t.date :start_processing
      t.date :end_processing

      t.timestamp :deleted_at
      t.timestamps null: false

    end
  end
end
