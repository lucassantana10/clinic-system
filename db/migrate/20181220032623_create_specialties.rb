class CreateSpecialties < ActiveRecord::Migration[5.2]
  def change
    create_table :specialties do |t|
      t.string :name
      t.text :description

      t.timestamp :deleted_at
      t.timestamps null: false
    end
  end
end
