class AddDiscountFromAppointments < ActiveRecord::Migration[5.2]
  def change
    add_column :appointments, :discount, :decimal
  end
end
