class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  def accessible_roles
    @accessible_roles = Role.where(deleted_at: nil).where.not(id: 1).accessible_by(current_ability, :read)
  end

  def accessible_genders
    @accessible_genders = Gender.accessible_by(current_ability, :read)
  end

  def accessible_specialties
    @accessible_specialties = Specialty.where(deleted_at: nil).where.not(id: 1).accessible_by(current_ability, :read)
  end

  def accessible_services
    @accessible_services = Service.where(deleted_at: nil).accessible_by(current_ability, :read)
  end

  def get_user
    @current_user = current_user
  end

end
