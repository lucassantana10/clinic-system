class PacientsController < ApplicationController
  before_action :set_pacient, only: [:edit, :update, :destroy]
  before_action :accessible_genders, :only => [:new, :edit, :update, :create]
  before_action :accessible_specialties, :only => [:index, :new, :edit, :update, :create]

  respond_to :html, :js, :json

  load_and_authorize_resource

  def index
    if params[:query].present?
      @pacients = Pacient.where(deleted_at: nil).search(params[:query])
                                                .order(name: :asc)
                                                .paginate(:page => params[:page],
                                                          :per_page => 30)
    else
      @pacients = Pacient.where(deleted_at: nil).order(name: :asc)
                                                .paginate(:page => params[:page],
                                                          :per_page => 30)
    end
    @appointment = Appointment.new
    respond_with(@pacients)
  end

  def new
    @pacient = Pacient.new

    respond_with(@pacient)
  end

  def edit
    @pacient.birthday = @pacient.birthday.try(:strftime,"%d/%m/%Y")
  end

  def create
    @pacient = Pacient.new(pacient_params)
   respond_to do |format|
      if @pacient.save
        flash[:notice] = 'Paciente criado com sucesso!'
        format.html { redirect_to(pacients_path)}
      else
        format.html { render action: "edit" }
      end
    end
  end

  def update

    respond_to do |format|
      if @pacient.update(pacient_params)

        flash[:notice] = 'Paciente editado com sucesso!'
        format.html { redirect_to(pacients_path) }
      else
        format.html { render action: "edit" }
      end
    end

  end

  def destroy
    @pacient.deleted_at = Time.now

    respond_to do |format|
      if @pacient.save!
        flash[:notice] = 'Paciente excluído com sucesso!'
        format.html { redirect_to(pacients_path) }
      end
    end

  end

  private
    def set_pacient
      @pacient = Pacient.find(params[:id])
    end

  def pacient_params
    params_list = [:email, :name, :cpf, :rg, :cep, :gender_id, :celphone, :birthday, :address,
                   :number, :complement, :profession, :fone, :celphone,
                   :marital_status, :nacionality, :indication,
                   :start_processing, :end_processing]
    params.require(:pacient).permit(params_list)
  end


end
