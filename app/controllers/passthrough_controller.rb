class PassthroughController < ApplicationController
  def index

    path = case current_user.role.name
    when 'Administrator', 'Dentista', 'Medico'
      appointments_path
    when 'Recepcionista'
      pacients_path
    # when 'Dentista'
    #   appointments_path
    # when 'Medico'
    #   appointments_path
    end

    redirect_to path unless path.nil?
  end
end
