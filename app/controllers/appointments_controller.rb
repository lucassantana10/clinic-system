class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:edit, :update, :destroy,
                                         :appointment_start,
                                         :appointment_payment,
                                         :appointment_update,
                                         :appointment_detail,
                                         :appointment_cancel,
                                         :edit_before_pay]
  before_action :accessible_services, :only => [:new, :create,
                                                :appointment_start,
                                                :appointment_payment]

  respond_to :html, :js, :json

  load_and_authorize_resource

  def index
    today = Time.zone.now.beginning_of_day..Time.zone.now.end_of_day
    if current_user.role?(:administrator) || current_user.role?(:recepcionista)
      @appointments = Appointment.where(status: 'wait', created_at: today)
                                 .order(created_at: :asc)
                                 .paginate(:page => params[:page],
                                           :per_page => 30)
    else
      @appointments = Appointment.where(status: 'wait', created_at: today,
                                        specialty_id: current_user.specialty_id)
                                 .order(created_at: :asc)
                                 .paginate(:page => params[:page],
                                           :per_page => 30)
    end

    respond_with(@appointments)
  end

  def new
    @appointment = Appointment.new

    respond_with(@appointment)
  end

  def edit
    @services_option = Service.where(specialty_id: current_user.specialty_id,
                                     deleted_at: nil)
                              .map{ |s| [ s.name, s.id ] }
  end

  def create
    @appointment = Appointment.new(appointment_params)

    # @appointment.specialty_id =
    @appointment.status = 'wait'

   respond_to do |format|
      if @appointment.save
        flash[:notice] = 'Consulta Marcada com sucesso!'
        format.html { redirect_to(appointments_path)}
      else
        format.html { render action: "edit" }
      end
    end
  end

  def update

    @appointment.status = "finished"
    @appointment.user_id = current_user.id

    AppointmentService.where(appointment_id: @appointment.id).destroy_all

    AppointmentService.transaction do
      params[:service_ids].each do | service_id |
        @appointment_services = AppointmentService.new
        @appointment_services.appointment_id = @appointment.id
        @appointment_services.service_id = service_id
        @appointment_services.save
      end
    end

    respond_to do |format|
      if @appointment.update(appointment_params)
        p @appointment
        flash[:notice] = 'Consulta Finalizada com sucesso!'
        format.html { redirect_to(appointments_path) }
      else
        format.html { render action: "edit" }
      end
    end

  end

  def appointment_start
    @appointment.status = "buzy"
    @appointment.user_id = current_user.id
    flash[:notice] = 'Paciente em atendimento!' if @appointment.save

     @appointment_services = @appointment.appointment_services.build

    @services_option = Service.where(specialty_id: current_user.specialty_id,
                                     deleted_at: nil)
                              .map{ |s| [ s.name, s.id ] }

    @appointments = Appointment.where(pacient_id: @appointment.pacient_id,
                                      specialty_id: current_user.specialty_id)
                               .where.not(status: ['buzy', 'wait'])
                               .order(created_at: :desc)
                               .limit(30)
  end

  def appointment_cancel

    @appointment.status = "canceled"
    @appointment.user_id = current_user.id

    respond_to do |format|
      if @appointment.save

        flash[:notice] = 'Consulta Cancelada com sucesso!'
        format.html { redirect_to(appointments_path) }
      else
        format.html { render action: "appointment_start" }
      end
    end

  end

  def appointment_finished

    today = Time.zone.now.beginning_of_day..Time.zone.now.end_of_day
    if current_user.role?(:administrator) || current_user.role?(:recepcionista)
    @appointments = Appointment.where(status: 'finished', created_at: today)
                               .order(payment_status: :desc)
                               .paginate(page: params[:page],
                                         per_page: 30)
    end
    respond_with(@appointments)
  end

  def appointment_payment
    @total = @appointment.services.sum(:price)
    @total += @appointment.price_optional if !@appointment.price_optional.nil?
    @total -= @appointment.discount if !@appointment.discount.nil?
  end

  def appointment_update

    @appointment.payment_status = "Payed"

    respond_to do |format|
      if @appointment.update(appointment_params)

        flash[:notice] = 'Pagamento Feito com sucesso!'
        format.html { redirect_to(appointment_finished_path) }
      else
        format.html { render action: "edit" }
      end
    end

  end

  def appointment_detail
    @total = @appointment.services.sum(:price)
    @total += @appointment.price_optional if !@appointment.price_optional.nil?
    @total -= @appointment.discount if !@appointment.discount.nil?
    respond_with(@appointment)
  end

  def appointment_history_all

    @appointments = Appointment.where(pacient_id: params[:pacient_id])
                               .order(created_at: :desc)
                               .paginate(:page => params[:page], :per_page => 30)
   @pacient = Pacient.find(params[:pacient_id]);
    respond_with(@appointments)

  end

  def appointments_of_day

    today = Time.zone.now.beginning_of_day..Time.zone.now.end_of_day
    if current_user.role?(:medico) || current_user.role?(:dentista)
      @appointments = Appointment.where(status: ['finished', 'buzy'], created_at: today)
                                 .order(created_at: :desc)
                                 .paginate(page: params[:page],
                                           per_page: 30)
    end
    respond_with(@appointments)
  end

  def edit_before_pay
    @services_option = Service.where(specialty_id: current_user.specialty_id,
                                     deleted_at: nil)
                              .map{ |s| [ s.name, s.id ] }

    @appointments = Appointment.where(pacient_id: @appointment.pacient_id,
                                      specialty_id: current_user.specialty_id)
                               .where.not(status: ['buzy', 'wait'])
                               .order(created_at: :desc)
                               .limit(30)

    respond_with(@appointment)
  end


  private
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

  def appointment_params
    params_list = [:pacient_id, :specialty_id, :service_id, :user_id, :price_optional, :discount,
                   :note, :payment_type, images: [], services_params: [:id]]
    params.require(:appointment).permit(params_list)
  end


end
