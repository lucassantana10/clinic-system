class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update, :destroy]
  before_action :accessible_roles, :only => [:index, :new, :edit, :update, :create]
  before_action :accessible_genders, :only => [:new, :edit, :update, :create]
  before_action :accessible_specialties, :only => [:new, :edit, :update, :create]

  respond_to :html, :js, :json

  load_and_authorize_resource

  def index
    if params[:query].present?
      @users = User.where(deleted_at: nil).where.not(id: 1).search(params[:query])
                                        .paginate(:page => params[:page],
                                                  :per_page => 30)
    else
      @users = User.where(deleted_at: nil).where.not(id: 1).paginate(:page => params[:page],
                                                    :per_page => 30)
    end
    respond_with(@users)
  end

  def new
    @user = User.new

    respond_with(@user)
  end

  def edit
    @user.birthday = @user.birthday.try(:strftime,"%d/%m/%Y")
  end

  def create
    @user = User.new(user_params)
    @user.set_avatar params[:user][:role_id], params[:user][:gender_id]
   respond_to do |format|
      if @user.save
        flash[:notice] = 'Usuario criado com sucesso!'
        format.html { redirect_to(users_path)}
      else
        format.html { render action: "edit" }
      end
    end
  end

  def update
    @user.set_avatar params[:user][:role_id], params[:user][:gender_id]

    respond_to do |format|
      if @user.update(user_params)

        flash[:notice] = 'Usuario editado com sucesso!'
        format.html { redirect_to(users_path) }
      else
        format.html { render action: "edit" }
      end
    end

  end

  def destroy
    @user.deleted_at = Time.now

    respond_to do |format|
      if @user.save!
        flash[:notice] = 'Usuario excluído com sucesso!'
        format.html { redirect_to(users_path) }
      end
    end

  end

  private
    def set_user
      @user = User.find(params[:id])
    end

  def user_params
    params_list = [:email, :reset_password_token, :name, :role_id, :gender_id, :specialty_id, :celphone, :birthday]
    params_list = params_list + [:password, :password_confirmation] unless params[:user][:password].blank?
    params.require(:user).permit(params_list)
  end


end
