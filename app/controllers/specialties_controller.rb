class SpecialtiesController < ApplicationController
  before_action :set_specialty, only: [:edit, :update, :destroy]

  respond_to :html, :js, :json

  load_and_authorize_resource

  def index
    if params[:query].present?
      @specialties = Specialty.where(deleted_at: nil).where.not(id: 1).search(params[:query])
                                                     .paginate(:page => params[:page],
                                                               :per_page => 30)
    else
      @specialties = Specialty.where(deleted_at: nil).where.not(id: 1).paginate(:page => params[:page],
                                                               :per_page => 30)
    end
    respond_with(@specialties)
  end

  def new
    @specialty = Specialty.new

    respond_with(@specialty)
  end

  def edit
  end

  def create
    @specialty = Specialty.new(specialty_params)

   respond_to do |format|
      if @specialty.save
        flash[:notice] = 'Especialidade criada com sucesso!'
        format.html { redirect_to(specialties_path)}
      else
        format.html { render action: "edit" }
      end
    end
  end

  def update

    respond_to do |format|
      if @specialty.update(specialty_params)

        flash[:notice] = 'Especialidade editada com sucesso!'
        format.html { redirect_to(specialties_path) }
      else
        format.html { render action: "edit" }
      end
    end

  end

  def destroy
    @specialty.deleted_at = Time.now

    respond_to do |format|
      if @specialty.save!
        flash[:notice] = 'Especialidade excluída com sucesso!'
        format.html { redirect_to(specialties_path) }
      end
    end

  end

  private
    def set_specialty
      @specialty = Specialty.find(params[:id])
    end

  def specialty_params
    params_list = [:name, :description]
    params.require(:specialty).permit(params_list)
  end


end
