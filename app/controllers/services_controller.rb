class ServicesController < ApplicationController
  before_action :set_service, only: [:edit, :update, :destroy]
  before_action :accessible_specialties, :only => [:new, :edit, :update, :create]

  respond_to :html, :js, :json

  load_and_authorize_resource

  def index
    if params[:query].present?
      @services = Service.where(deleted_at: nil).search(params[:query])
                                                .paginate(:page => params[:page],
                                                          :per_page => 30)
    else
      @services = Service.where(deleted_at: nil).paginate(:page => params[:page],
                                                          :per_page => 30)
    end
    respond_with(@services)
  end

  def new
    @service = Service.new

    respond_with(@service)
  end

  def edit
  end

  def create
    @service = Service.new(service_params)

   respond_to do |format|
      if @service.save
        flash[:notice] = 'Serviço criado com sucesso!'
        format.html { redirect_to(services_path)}
      else
        format.html { render action: "edit" }
      end
    end
  end

  def update

    respond_to do |format|
      if @service.update(service_params)

        flash[:notice] = 'Serviço editado com sucesso!'
        format.html { redirect_to(services_path) }
      else
        format.html { render action: "edit" }
      end
    end

  end

  def destroy
    @service.deleted_at = Time.now

    respond_to do |format|
      if @service.save!
        flash[:notice] = 'Serviço excluído com sucesso!'
        format.html { redirect_to(services_path) }
      end
    end

  end

  private
    def set_service
      @service = Service.find(params[:id])
    end

  def service_params
    params_list = [:name, :price, :description, :specialty_id]
    params.require(:service).permit(params_list)
  end


end
