class Service < ApplicationRecord
  has_and_belongs_to_many :appointments, :join_table => :services_appointments

  include PgSearch
  pg_search_scope :search, against: [:name]

 belongs_to :specialty
 belongs_to :appointment, inverse_of: :services, optional: true

 has_many :appointment_services
 has_many :appointments, through: :appointment_services


 validates_presence_of :name, :specialty_id, :price

end
