class Gender < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :genders_users
  has_and_belongs_to_many :pacients, :join_table => :genders_pacients

  default_scope { order('name ASC') }
end
