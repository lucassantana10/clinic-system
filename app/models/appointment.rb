class Appointment < ActiveRecord::Base
  # has_and_belongs_to_many :users, :join_table => :appointments_users
  # has_and_belongs_to_many :appointment_services, :join_table => :appointments_services
  # has_and_belongs_to_many :pacients, :join_table => :appointments_pacients

  belongs_to :user, optional: true
  belongs_to :pacient
  belongs_to :specialty, optional: true

  has_many :appointment_services
  has_many :services, through: :appointment_services

  # accepts_nested_attributes_for :appointment_services, reject_if: proc { |attributes| attributes[:id].blank? }

  has_many_attached :images

end
