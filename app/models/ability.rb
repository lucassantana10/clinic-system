class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user

    # @todo: change :manage to especif methods

    can :manage, :all if user.role? :administrator

    if user.role? :recepcionista
      can :manage, [Pacient, Service, Specialty]
      can [:index, :create, :appointment_finished, :appointment_finished_update,
           :appointment_detail, :appointment_payment, :appointment_update], Appointment
      can [:index, :create, :update], User
      can [:read], [Gender, Service, Specialty, Role]
    end

    if user.role? :dentista
      can [:index, :update, :appointment_start, :appointment_cancel,
           :appointment_history_all, :appointments_of_day, :edit_before_pay], Appointment
      can [:read], Pacient
      can [:index, :update], User
    end

    if user.role? :medico
      can [:index, :update, :appointment_start, :appointment_cancel,
           :appointment_history_all, :appointments_of_day, :edit_before_pay], Appointment
      can [:index, :update], User
    end

    if user.role? :laboratorio
      can [:index, :update, :appointment_start, :appointment_cancel,
           :appointment_history_all, :appointment_of_day, :edit_before_pay], Appointment
      can [:index, :update], User
    end


  end
end
