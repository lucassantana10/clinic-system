class AppointmentService < ApplicationRecord

 belongs_to :appointment
 belongs_to :service

 validates_presence_of :appointment_id, :service_id

end
