class Pacient < ApplicationRecord
    has_and_belongs_to_many :appointments, :join_table => :pacients_appointments
   include PgSearch
   pg_search_scope :search, against: [:name, :email]

   belongs_to :gender
   has_many :appointments

   validates_presence_of :name, :gender_id, :celphone, :birthday,
                         :marital_status

end
