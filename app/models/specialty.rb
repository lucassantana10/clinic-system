class Specialty < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :specialties_users
  has_and_belongs_to_many :services, :join_table => :specialties_services
  # has_and_belongs_to_many :appointments, :join_table => :specialties_appointments
  has_many :appointments
  include PgSearch
  pg_search_scope :search, against: [:name, :description]

  default_scope { order('name ASC') }
end
