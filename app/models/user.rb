class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

# has_and_belongs_to_many :appointments, :join_table => :users_appointments

 belongs_to :role
 belongs_to :gender
 belongs_to :specialty, optional: true
 has_many :appointments

 validates_presence_of :name, :role_id, :specialty_id, :gender_id

 default_scope { order('name ASC') }

 include PgSearch
 pg_search_scope :search, against: [:name, :email]

 def role?(role_sym)
   self.role.name.downcase.gsub(/( )/, '_').to_sym == role_sym
 end

 def is_admin?
   role? :administrator
 end

 def set_avatar role_id, gender_id
    case role_id
        when '1'
            self.avatar = 'owner-developer.png'
        when '2'
            self.avatar = gender_id == '1' ? 'doctor-male.png' : 'doctor-female.png'
        when '3'
            self.avatar = gender_id == '1' ? 'doctor-male.png' : 'doctor-female.png'
        when '4'
            self.avatar = gender_id == '1' ? 'lab-male.png' : 'lab-female.png'
        when '5'
            self.avatar = gender_id == '1' ? 'receptionist-male.png' : 'receptionist-female.png'
    end
 end

end
